# coding=utf-8

from PIL import Image
import numpy as np
# import cv2
import os
import base64
import io
import requests
import logging

__all__ = ['convert', 'load']
logger = logging.getLogger(__name__)

"""
Приоритет использования входных параметров для получения данных изображения:
1. image
2. filepath
3. url

При указании filepath или url, скрипт получает изображение, преобразуя в формат PIL.Image


Маршруты преобразования:
без изменения цвета
pil -> pil.conv -> pil
pil -> pil.conv -> b64
pil -> pil.conv -> np
b64 -> pil -> pil.conv
b64 -> pil -> pil.conv -> b64
b64 -> pil -> pil.conv -> np
np -> pil -> pil.conv
np -> pil -> pil.conv -> b64
np -> pil -> pil.conv -> np

Во всех входных форматах кроме np цветовой профиль определяется автоматически
Для np по умолчанию заданы цветовые профили (L, LA, RGB, RGBA) в зависимости от количества каналов.
PIL определяет все цветовые профили кроме BGR
поддерживаемые для конвертации цветовые пространства np (L, LA, RGB, RGBA, BGR)


Описание цветовых профилей:
1 (1-bit pixels, black and white, stored with one pixel per byte)
L (8-bit pixels, black and white)
P (8-bit pixels, mapped to any other mode using a color palette)
RGB (3x8-bit pixels, true color)
BGR same as RGB, but channel-mirrored, uses for cv
YCbCr (3x8-bit pixels, color video format) Note that this refers to the JPEG, and not the ITU-R BT.2020, standard
RGBA (4x8-bit pixels, true color with transparency mask)
CMYK (4x8-bit pixels, color separation)
LAB (3x8-bit pixels, the L*a*b color space)
HSV (3x8-bit pixels, Hue, Saturation, Value color space)
I (32-bit signed integer pixels)
F (32-bit floating point pixels)
"""


MODES = ('L', 'P', 'LA', 'RGB', 'BGR', 'RGBA', 'CMYK', 'YCbCr', 'HSV')  # 'LAB', 'I', 'F', '1'
NP_MODES = ('L', 'LA', 'RGB', 'RGBA', 'BGR')  # цветовые профили по умолчанию для каждого количества каналов
TYPES = ('np', 'pil', 'b64', 'cv')  # cv = alias for np (bgr)


def _errlog(text, code):
    logger.error(text)
    if code == 1:
        raise AttributeError(text)
    elif code == 2:
        raise ValueError(text)
    elif code == 3:
        raise RuntimeError(text)


def _convert_mode(image=None, input_mode=None, output_mode=None):
    """
    Преобразует цветовое пространство
    :param image: pil.image
    :param input_mode: цветовой профиль входного изображения, см. MODES
    :param output_mode: цветовой профиль выходного изображения, см. MODES
    :return:
    """
    logger.debug(f'{input_mode} -> {output_mode}')

    if input_mode == output_mode:
        return image

    def rgb_bgr(img):
        img = img.convert('RGB')
        img = np.array(img)
        img = img[:, :, ::-1].copy()
        return Image.fromarray(img)

    if input_mode == 'BGR':
        image = rgb_bgr(image)

    if output_mode == 'BGR':
        image = image.convert('RGB')
        image = rgb_bgr(image)
    else:
        image = image.convert(output_mode)
    return image


def _convert_type(image=None, input_type=None, output_type=None):
    """
    преобразует тип
    :param image: изображение
    :param input_type: тип входного изображения, см. TYPES
    :param output_type: тип выходного изображения, см. TYPES
    :return:
    """
    logger.debug(f'{input_type} -> {output_type}')

    if input_type == output_type:
        return image

    def np_pil(img):
        return Image.fromarray(img)

    def b64_pil(img):
        buff = io.BytesIO(base64.b64decode(img))
        return Image.open(buff).copy()

    def pil_np(img):
        img = np.array(img)
        return img

    def pil_b64(img):
        buff = io.BytesIO()
        img.save(buff, format="JPEG")
        return base64.b64encode(buff.getvalue())

    funcs = {'np_pil': np_pil, 'b64_pil': b64_pil, 'pil_np': pil_np, 'pil_b64': pil_b64}

    if 'pil' not in (input_type, output_type):
        image = funcs[f'{input_type}_pil'](image)
        return funcs[f'pil_{output_type}'](image)
    else:
        return funcs[f'{input_type}_{output_type}'](image)


def convert(image, output_type, output_mode=None, input_mode=None, input_type=None):
    """
    Преобразует изображение, меняя тип и цветовой профиль
    :param image: входное изображение
    :param input_mode: цветовой профиль входного изображения, определяется автоматически для b64 и pil, см. MODES
    :param input_type: тип входного изображения, определяется автоматически, см. TYPES
    :param output_mode: цветовой профиль выходного изображения, см. MODES
    :param output_type: тип выходного изображения, см. TYPES
    :return:
    """
    def np_detect_mode(np_image):
        if len(np_image.shape) == 2:
            return NP_MODES[0]
        if len(np_image.shape) == 3:
            if 0 < np_image.shape[2] < 5:
                return NP_MODES[np_image.shape[2] - 1]
            else:
                _errlog('Передано изображение в неподдерживаемом формате!', 2)

    # реализация алиаса для cv
    if input_type == 'cv':
        input_mode = 'BGR'
        input_type = 'np'
    if output_type == 'cv':
        output_mode = 'BGR'
        output_type = 'np'

    # автоопределение типа и цветового профиля входного изображения
    if isinstance(image, np.ndarray):
        input_type = 'np'
        if input_mode is None:
            input_mode = np_detect_mode(image)
            logger.info(f'Входной цветовой профиль автоматически определен как {input_mode}')
        elif input_mode not in NP_MODES:
            _errlog('Задан неверный входящий цветовой профиль!', 1)
    else:
        if isinstance(image, Image.Image):
            input_type = 'pil'
            input_mode = image.mode
        elif isinstance(image, bytes):
            input_type = 'b64'
            tmp_image = _convert_type(image, input_type, 'pil')
            input_mode = tmp_image.mode
            del tmp_image
        else:
            _errlog('Передан неподдерживаемый тип входного изображения!', 2)
        if input_mode not in MODES:
            _errlog(f'Передан неподдерживаемый входной цветовой профиль {input_mode}!', 1)

    if output_mode is None:
        output_mode = input_mode
    elif output_mode not in MODES:
        _errlog(f'Передан неподдерживаемый выходной цветовой профиль {output_mode}!', 1)


    logger.info(f'{input_type}({input_mode}) -> {output_type}({output_mode})')

    # если нужна конвертация цветового профиля
    if input_mode != output_mode:
        # преобразуем тип к pil для конвертации, если нужно
        if input_type != 'pil':
            image = _convert_type(image, input_type, 'pil')
        # конвертируем цветовой профиль через pil
        image = _convert_mode(image, input_mode, output_mode)
        # преобразуем тип к выходному, если нужно
        if output_type != 'pil':
            image = _convert_type(image, 'pil', output_type)
        return image

    # если нужна конвертация типа
    if input_type != output_type:
        return _convert_type(image, input_type, output_type)

    return image


def load(imagepath=None, url=None, output_type='pil', output_mode=None):
    """
    Выполняет загрузку изображения и преобразование к нужному формату данныъ
    :param imagepath: путь к изображению
    :param url: сетевой адрес для загрузки изображения
    :param output_mode: цветовой профиль выходного изображения, см. MODES
    :param output_type: тип выходного изображения, см. TYPES
    :return:
    """
    # првоеряем поддерку типа
    if output_type not in TYPES:
        _errlog('Передан неподдерживаемый тип выходного изображения', 1)

    # приоритет загрузки в порядке усложнения
    if imagepath:
        # загружаем изображение с диска
        if os.path.exists(imagepath):
            image = Image.open(imagepath).copy()
        else:
            _errlog('Неверно указан путь к файлу, либо файл не существует', 1)

    elif url:
        # загружаем изображение по адресу
        response = requests.get(url)
        image = Image.open(io.BytesIO(response.content))

    else:
        _errlog('Не передан ни один параметр с для источника данных!', 1)

    # преобразование изображения к выходному формату
    image = convert(image, output_type, output_mode=output_mode)
    return image
