# coding=utf-8
"""
Определяем тип документа и конвертируем его в jpg
"""
import magic
from psd_tools import PSDImage
from PIL import Image
import PyPDF4
import os

import logging

logger = logging.getLogger(__name__)

__all__ = ['recognize_type', 'convert_to_jpg']


def _errlog(text, code):
    logger.error(text)
    if code == 1:
        raise AttributeError(text)
    elif code == 2:
        raise ValueError(text)
    elif code == 3:
        raise RuntimeError(text)


# rename jpg
def _jpg_jpg(imagepath):
    # тупо переименовываем
    Image.open(imagepath).save(imagepath + '.jpg', "JPEG", quality=100)
    return [imagepath + '.jpg']


# png -> jpg
def _png_jpg(imagepath):
    img = Image.open(imagepath)
    white_background = Image.new('RGB', img.size, (255, 255, 255))
    white_background.paste(img, mask=img)
    white_background.convert('RGB').save(imagepath + '.jpg', "JPEG", quality=100)
    return [imagepath + '.jpg']


# bmp -> jpg
def _bmp_jpg(imagepath):
    img = Image.open(imagepath)
    img.convert('RGB').save(imagepath + '.jpg', "JPEG", quality=100)
    return [imagepath + '.jpg']


# gif -> jpg
def _gif_jpg(imagepath):
    return _bmp_jpg(imagepath)


# tiff -> jpg
def _tiff_jpg(imagepath):
    im = Image.open(imagepath)
    # im.convert("RGB")
    im.thumbnail(im.size)
    im.convert('RGB').save(imagepath + '.jpg', "JPEG", quality=100)
    return [imagepath + '.jpg']


# psd -> jpg
def _psd_jpg(imagepath):
    psd = PSDImage.open(imagepath)
    psd.compose().save(imagepath + '.jpg', "JPEG", quality=100)
    return [imagepath + '.jpg']


# pdf -> jpg
def _pdf_jpg(pdfpath):
    pdf_file = PyPDF4.PdfFileReader(open(pdfpath, "rb"))

    img_index = 0
    img_collection = []
    for i in range(0, pdf_file.getNumPages()):
        page = pdf_file.getPage(i)
        if '/XObject' in page['/Resources']:
            xObject = page['/Resources']['/XObject'].getObject()
            for obj in xObject:
                if xObject[obj]['/Subtype'] == '/Image':
                    img_index += 1
                    size = (xObject[obj]['/Width'], xObject[obj]['/Height'])
                    data = xObject[obj].getData()
                    mode = "RGB" if xObject[obj]['/ColorSpace'] == '/DeviceRGB' else "P"
                    img_name = pdfpath + str(img_index) + ".jpg"
                    if '/Filter' in xObject[obj]:
                        if xObject[obj]['/Filter'] == '/FlateDecode':
                            img = Image.frombytes(mode, size, data)
                            img.convert("RGBA")
                            white_background = Image.new('RGB', img.size, (255, 255, 255))
                            white_background.paste(img, mask=img)
                            white_background.convert("RGB").save(img_name, "JPEG", quality=100)
                            img_collection.append(img_name)
                        elif xObject[obj]['/Filter'] == '/DCTDecode':
                            img = open(img_name, "wb")
                            img.write(data)
                            img.close()
                            img_collection.append(img_name)
                        elif xObject[obj]['/Filter'] == '/JPXDecode':
                            img_name = pdfpath + str(img_index) + ".jp2"
                            img = open(img_name, "wb")  # пока никак не конвертировать
                            img.write(data)
                            img.close()
                            img_collection.append(img_name)
                        elif xObject[obj]['/Filter'] == '/CCITTFaxDecode':
                            tmp_name = pdfpath + str(img_index)
                            img = open(tmp_name, "wb")
                            img.write(data)
                            img.close()
                            #  костыльная конвертация
                            img_collection.append(_tiff_jpg(tmp_name))
                            os.remove(tmp_name)
                    else:
                        img = Image.frombytes(mode, size, data)
                        img.convert("RGB").save(img_name, "JPEG", quality=100)
                        img_collection.append(img_name)
    return img_collection if img_collection else None


def recognize_type(filepath):
    """
    :param filepath:
    :return:
        application/pdf
        image/jpeg
        image/png
        image/gif
        image/tiff
        image/bmp
        image/x-ms-bmp
        image/vnd.adobe.photoshop
        None
    """
    result = magic.from_file(filepath, mime=True)
    return result


def convert_to_jpg(filepath):
    """
    Конвертируем любой известный файл в изображения
    :param filepath:
    :return: [filepath1, filepath2, ...] или None если не удалось конвертировать
    """
    filetype = recognize_type(filepath)
    logger.info('тип файла: ' + filetype)
    if filetype:
        if filetype == "application/pdf":
            return _pdf_jpg(filepath)
        elif filetype == "image/jpeg":
            return _jpg_jpg(filepath)
        elif filetype == "image/png":
            return _png_jpg(filepath)
        elif filetype == "image/gif":
            return _gif_jpg(filepath)
        elif filetype == "image/tiff":
            return _tiff_jpg(filepath)
        elif filetype in ["image/bmp", "image/x-ms-bmp"]:
            return _bmp_jpg(filepath)
        elif filetype == "image/vnd.adobe.photoshop":
            return _psd_jpg(filepath)
    else:
        logger.info('Не удалось определить тип файла')
        return None
