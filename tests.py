# coding=utf-8

from PIL import Image
import cv2
from converter import file, image
import logging
import sys
import os


logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)


# тесты
# file.convert_to_image('test/pdf1')  # текст
# file.convert_to_image('test/pdf2')  # картинка
# file.convert_to_image('test/pdf3')  # две страницы с фотографиями
# file.convert_to_image('test/png1')
# file.convert_to_image('test/tiff1')
# file.convert_to_image('test/bmp1')
# file.convert_to_image('test/psd1')

# LOAD
# filepath
# im = image.load(imagepath='test_images/L.jpg', output_mode='RGB')
# im = image.load(imagepath='test_images/1bit.gif', output_mode='RGB')
# im = image.load(imagepath='test_images/LA.png', output_mode='RGB')
# im = image.load(imagepath='test_images/RGB.jpg', output_mode='RGB')
# im = image.load(imagepath='test_images/LAB.tif')
# im = image.load(imagepath='test_images/RGBA.png', output_mode='RGB')
# im = image.load(imagepath='test_images/CMYK.tif', output_mode='RGB')
# im.show('path')
# del im

# url
# im = image.load(url='http://i.dailymail.co.uk/i/pix/2015/09/01/18/2BE1E88B00000578-3218613-image-m-5_1441127035222.jpg')
# im.show('url')
# del im


# CONVERT pil -> *
# L
# im_pil = Image.open('test_images/L.jpg')  # im == pil
# _ = image.convert(im_pil, 'pil')  # im == pil
# im_np = image.convert(im_pil, 'np')  # im == np
# im_cv = image.convert(im_pil, 'cv')  # im == np
# im_b64 = image.convert(im_pil, 'b64')  # im == pil

# LA
# im = Image.open('test_images/LA.png')
#
# # RGB, BGR
# rgb = image.convert(imagepath='test_images/RGB.jpg')
# bgr = image.convert(imagepath='test_images/RGB.jpg', output_mode='BGR')
#
# # RGBA
# grba = image.convert(imagepath='test_images/RGBA.png')
#
# # CMYK
# cmyk = image.convert(imagepath='test_images/CMYK.jpg')
# # tmp = image.load(imagepath='test_images/LAB.tif')
#
# # 1, P
# tmp = image.convert(imagepath='test_images/1bit.bmp')
#

# import base64
# from io import BytesIO
#
# buffered = BytesIO()
# im_pil = Image.open('test_images/1bit.png')  # im == pil
# im_pil.save(buffered, format="JPEG")
# img_str = base64.b64encode(buffered.getvalue())
# # print(type(img_str))
#
# img = image.convert(img_str, 'np', 'RGB')
# print(img.shape)
# cv2.imshow('1', img)
# cv2.waitKey()

folder_path = '/Users/sergey/dev_dir/Ida-Ten/face_processor/TEST/'
files = [f for f in os.listdir(folder_path)
         if os.path.isfile(folder_path + f) and f[0] != '.']
print(files)
for f in files:
    filename, file_extension = os.path.splitext(f)
    if not file_extension:
        filename = folder_path + filename
        print(filename)
        os.rename(filename, filename + '.' + file.recognize_type(filename).split('/')[1])
